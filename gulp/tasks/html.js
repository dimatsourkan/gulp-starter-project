const gulp = require('gulp');
const cachebust = require('gulp-cache-bust');
const mode = require('gulp-mode')();
const paths = require('../paths');
const plumber = require('gulp-plumber');
const fileinclude = require('gulp-file-include');

const cachebustConfig = {
  type: 'timestamp',
};

const html = () => {

  return gulp
    .src(paths.src.html)
    .pipe(plumber())
    .pipe(fileinclude())
    .pipe(mode.production(cachebust(cachebustConfig)))
    .pipe(gulp.dest(paths.build.html));
};

module.exports = html;
