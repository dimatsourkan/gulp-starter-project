const gulp = require('gulp');
const uglify = require('gulp-uglify');
const mode = require('gulp-mode')();
const paths = require('../paths');
const browserify = require('browserify');
const babelify = require('babelify');
const buffer = require('vinyl-buffer');
const source = require('vinyl-source-stream');

const scripts = () => {
  return browserify({ entries: paths.src.js })
    .transform(babelify.configure({
      presets: ["@babel/preset-env"],
      sourceType: "module"
    }))
    .bundle()
    .pipe(source('app.js'))
    .pipe(buffer())
    .pipe(mode.production(uglify()))
    .pipe(gulp.dest(paths.build.js));
};

module.exports = scripts;
